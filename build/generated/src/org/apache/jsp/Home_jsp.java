package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("        <meta name=\"keywords\" content=\"Bootstrap, Landing page, Template, Registration, Landing\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\n");
      out.write("        <meta name=\"author\" content=\"UIdeck\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/line-icons.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/owl.carousel.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/owl.theme.default.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/slicknav.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/animate.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/main.css\">    \n");
      out.write("        <link rel=\"stylesheet\" href=\"assets/css/responsive.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <header id=\"home\" class=\"hero-area\"> \n");
      out.write("            ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Menu.jsp", out, false);
      out.write("\n");
      out.write("                <div class=\"container\">      \n");
      out.write("                    <div class=\"row space-100 justify-content-center\">\n");
      out.write("                        <div class=\"col-lg-10 col-md-12 col-xs-12\">\n");
      out.write("                            <div class=\"contents\">\n");
      out.write("                                <h2 class=\"head-title\">Find the job that fits your life</h2>\n");
      out.write("                                <div class=\"job-search-form\">\n");
      out.write("                                    <!--                                    <form action=\"searchjob\" method=\"post\">\n");
      out.write("                                                                            <div class=\"row\">\n");
      out.write("                                                                                <div class=\"col-lg-5 col-md-6 col-xs-12\">\n");
      out.write("                                                                                    <div class=\"form-group\">\n");
      out.write("                                                                                        <input class=\"form-control\" type=\"text\" name=\"tittle\" placeholder=\"Job Title\">\n");
      out.write("                                                                                    </div>\n");
      out.write("                                                                                </div>\n");
      out.write("                                                                                <div class=\"col-lg-3 col-md-6 col-xs-12\">\n");
      out.write("                                                                                    <div class=\"form-group\">\n");
      out.write("                                                                                        <div class=\"search-category-container\">\n");
      out.write("                                                                                            <label class=\"styled-select\">\n");
      out.write("                                                                                                <select name=\"location\">\n");
      out.write("                                                                                                    <option value=\"NONE\">Locations</option>\n");
      out.write("                                                                                                    <option value=\"HA NOI\">HA NOI</option>\n");
      out.write("                                                                                                    <option value=\"HO CHI MINH\">HO CHI MINH</option>\n");
      out.write("                                                                                                    <option value=\"HAI PHONG\">HAI PHONG</option>\n");
      out.write("                                                                                                    <option value=\"CAN THO\">CAN THO</option>\n");
      out.write("                                                                                                    <option value=\"DA NANG\">DA NANG</option>\n");
      out.write("                                                                                                    <option value=\"BIEN HOA\">BIEN HOA</option>\n");
      out.write("                                                                                                    <option value=\"THU DUC\">THU DUC</option>\n");
      out.write("                                                                                                    <option value=\"HAI DUONG\">HAI DUONG</option>\n");
      out.write("                                                                                                    <option value=\"THUAN AN\">THUAN AN</option>\n");
      out.write("                                                                                                    <option value=\"HUE\">HUE</option>\n");
      out.write("                                                                                                </select>\n");
      out.write("                                                                                            </label>\n");
      out.write("                                                                                        </div>\n");
      out.write("                                                                                        <i class=\"lni-map-marker\"></i>\n");
      out.write("                                                                                    </div>\n");
      out.write("                                                                                </div>\n");
      out.write("                                                                                <div class=\"col-lg-3 col-md-6 col-xs-12\">\n");
      out.write("                                                                                    <div class=\"form-group\">\n");
      out.write("                                                                                        <div class=\"search-category-container\">\n");
      out.write("                                                                                            <label class=\"styled-select\">\n");
      out.write("                                                                                                <select name=\"category\">\n");
      out.write("                                                                                                    <option value=\"0\">All Categories</option>\n");
      out.write("                                ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                        </select>\n");
      out.write("                    </label>\n");
      out.write("                </div>\n");
      out.write("                <i class=\"lni-layers\"></i>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-lg-1 col-md-6 col-xs-12\">\n");
      out.write("            <button type=\"submit\" class=\"button\"><i class=\"lni-search\"></i></button>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>-->\n");
      out.write("                                <form id=\"form-box-search\" class=\"box-main__form-search position-relative\" action=\"searchjob\" method=\"post\" autocomplete=\"off\">\n");
      out.write("                                    <div class=\"box-search\"><div class=\"cps-group-input\"><div class=\"input-group-btn\"><i class=\"fas fa-search\"></i></div><input type=\"text\" name=\"title\" id=\"search\" class=\"cps-input cta-search\" placeholder=\"Bạn cần tìm gì?\" maxlength=\"128\"></div></div>\n");
      out.write("                                    <div id=\"search_autocomplete\" class=\"box-search-result search-autocomplete\"></div>\n");
      out.write("                                    <script defer>\n");
      out.write("                                        window.addEventListener('DOMContentLoaded', function () {\n");
      out.write("                                            var searchForm = new Varien.searchForm('form-box-search', 'search', '');\n");
      out.write("                                            searchForm.initAutocomplete('https://cellphones.com.vn/catalogsearch/ajax/suggest/', 'search_autocomplete');\n");
      out.write("\n");
      out.write("                                            jQuery('#search').on('input', function () {\n");
      out.write("                                                if (this.value === '') {\n");
      out.write("                                                    jQuery('#search_autocomplete').hide();\n");
      out.write("                                                } else {\n");
      out.write("                                                    jQuery('#search_autocomplete').show();\n");
      out.write("                                                }\n");
      out.write("                                            });\n");
      out.write("\n");
      out.write("                                        })\n");
      out.write("                                        function showAjaxSearchResult() {\n");
      out.write("                                            jQuery('#search_autocomplete').show();\n");
      out.write("                                        }\n");
      out.write("                                        jQuery(\".box-main__form-search #search\").on(\"focus\", function () {\n");
      out.write("                                            // jQuery('#search_autocomplete').show();\n");
      out.write("                                            jQuery(\".header-overlay\").addClass(\"active\");\n");
      out.write("                                        }).blur(function () {\n");
      out.write("                                            // jQuery('#search_autocomplete').hide();\n");
      out.write("                                            jQuery(\".header-overlay\").removeClass(\"active\");\n");
      out.write("                                        });\n");
      out.write("                                    </script>\n");
      out.write("                                </form>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div> \n");
      out.write("            </div>             \n");
      out.write("        </header>\n");
      out.write("        <section class=\"category section bg-gray\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"section-header\">  \n");
      out.write("                    <h2 class=\"section-title\">Browse Categories</h2>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    ");
      if (_jspx_meth_c_forEach_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("        <div id=\"browse-jobs\" class=\"section bg-gray\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-lg-6 col-md-12 col-sm-12\">\n");
      out.write("                        <div class=\"text-wrapper\">\n");
      out.write("                            <div>\n");
      out.write("                                <h3>7,000+ Browse Jobs</h3>\n");
      out.write("                                <p>Search all the open positions on the web. Get your own personalized salary estimate. Read reviews on over 600,000 companies worldwide. The right job is out there.</p>\n");
      out.write("                                <a class=\"btn btn-common\" href=\"alljob\">Search jobs</a>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-lg-6 col-md-12 col-sm-12\">\n");
      out.write("                        <div class=\"img-thumb\">\n");
      out.write("                            <img class=\"img-fluid\" src=\"assets/img/search.png\" alt=\"\">\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <section class=\"how-it-works section\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"section-header\">  \n");
      out.write("                    <h2 class=\"section-title\">How It Works?</h2>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">\n");
      out.write("                        <div class=\"work-process\">\n");
      out.write("                            <span class=\"process-icon\">\n");
      out.write("                                <i class=\"lni-user\"></i>\n");
      out.write("                            </span>\n");
      out.write("                            <h4>Create an Account</h4>\n");
      out.write("                            <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers find place best.</p>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">\n");
      out.write("                        <div class=\"work-process step-2\">\n");
      out.write("                            <span class=\"process-icon\">\n");
      out.write("                                <i class=\"lni-search\"></i>\n");
      out.write("                            </span>\n");
      out.write("                            <h4>Search Jobs</h4>\n");
      out.write("                            <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers find place best.</p>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">\n");
      out.write("                        <div class=\"work-process step-3\">\n");
      out.write("                            <span class=\"process-icon\">\n");
      out.write("                                <i class=\"lni-cup\"></i>\n");
      out.write("                            </span>\n");
      out.write("                            <h4>Apply</h4>\n");
      out.write("                            <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers find place best.</p>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("        <section id=\"latest-jobs\" class=\"section bg-gray\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"section-header\">  \n");
      out.write("                    <h2 class=\"section-title\">Latest Jobs</h2>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    ");
      if (_jspx_meth_c_forEach_2(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                    <div class=\"col-12 text-center mt-4\">\n");
      out.write("                        <a href=\"alljob\" class=\"btn btn-common\">Browse All Jobs</a>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Footer.jsp", out, false);
      out.write("\n");
      out.write("        <a href=\"#\" class=\"back-to-top\">\n");
      out.write("            <i class=\"lni-arrow-up\"></i>\n");
      out.write("        </a> \n");
      out.write("        <div id=\"preloader\">\n");
      out.write("            <div class=\"loader\" id=\"loader-1\"></div>\n");
      out.write("        </div>\n");
      out.write("        <script src=\"assets/js/jquery-min.js\"></script>\n");
      out.write("        <script src=\"assets/js/popper.min.js\"></script>\n");
      out.write("        <script src=\"assets/js/owl.carousel.min.js\"></script>     \n");
      out.write("        <script src=\"assets/js/jquery.slicknav.js\"></script>     \n");
      out.write("        <script src=\"assets/js/jquery.counterup.min.js\"></script>      \n");
      out.write("        <script src=\"assets/js/waypoints.min.js\"></script>     \n");
      out.write("        <script src=\"assets/js/form-validator.min.js\"></script>\n");
      out.write("        <script src=\"assets/js/contact-form-script.js\"></script>   \n");
      out.write("        <script src=\"assets/js/main.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${allCategory}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("o");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                    <option value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write('"');
          out.write('>');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</option>\n");
          out.write("                                ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent(null);
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${allCategory}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_1.setVar("o");
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                        <div class=\"col-lg-3 col-md-6 col-xs-12 f-category\">\n");
          out.write("                            <a href=\"alljob?CategoryID=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">\n");
          out.write("                                <div class=\"icon bg-color-1\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.icon}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</div>\n");
          out.write("                                <h3>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</h3>\n");
          out.write("                                <p>(");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.countjob}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write(")</p>\n");
          out.write("                            </a>\n");
          out.write("                        </div>\n");
          out.write("                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_2 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_2.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_2.setParent(null);
    _jspx_th_c_forEach_2.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${listJ}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_2.setVar("o");
    int[] _jspx_push_body_count_c_forEach_2 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_2 = _jspx_th_c_forEach_2.doStartTag();
      if (_jspx_eval_c_forEach_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                        <div class=\"col-lg-6 col-md-12 col-xs-12\">\n");
          out.write("                            <div class=\"jobs-latest\">\n");
          out.write("                                <div class=\"img-thumb\">\n");
          out.write("                                    <img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.logo}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" style=\"max-width: 100px; min-height: 100px\" alt=\"\">\n");
          out.write("                                </div>\n");
          out.write("                                <div class=\"content\">\n");
          out.write("                                    <h3><a href=\"detail?JobID=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.jId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write('"');
          out.write('>');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.jName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</a></h3>\n");
          out.write("                                    <p class=\"brand\">");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.company}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\n");
          out.write("                                    <div class=\"tags\">  \n");
          out.write("                                        <span><i class=\"lni-map-marker\"></i>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.location}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span>  \n");
          out.write("                                        <span>$");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${o.salary}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span> \n");
          out.write("                                    </div>\n");
          out.write("                                    <span class=\"full-time\">Full Time</span>\n");
          out.write("                                </div>\n");
          out.write("                            </div>\n");
          out.write("                        </div>\n");
          out.write("                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_2.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_2[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_2.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_2.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_2);
    }
    return false;
  }
}
